package org.foxat.pviewgui.test;

import org.foxat.pviewgui.PGView;
import org.foxat.pviewgui.ProcessingGUI;
import org.foxat.pviewgui.action.DrawAction;
import org.foxat.pviewgui.element.PGButton;
import org.foxat.pviewgui.interfaces.Parent;
import processing.core.PApplet;
import processing.core.PFont;

import java.awt.*;

/**
 * General Tests
 * @author Christian Schliz
 * */
public class Test extends PApplet implements Parent {

    public void settings() {
        size(400, 400);
    }

    public ProcessingGUI gui;

    MenuView menu;
    HelpView help;
    IngameView ingame;

    DrawAction buttonStyle;

    public void setup() {
        PFont a = createFont("Comic Sans MS", 16);
        textFont(a);

        buttonStyle = (b, p) -> {
            p.pushStyle();

            if(p.mousePressed && b.touchUpInside(p.mouseX, p.mouseY)) {
                p.fill(0, 54, 140);
            } else {
                p.fill(255);
            }
            p.strokeWeight(2);
            p.stroke(0, 0, 72);
            p.rectMode(CENTER);
            p.rect(b.x, b.y, b.w, b.h);

            if(p.mousePressed && b.touchUpInside(p.mouseX, p.mouseY)) {
                p.fill(255);
            } else {
                p.fill(0, 0, 72);
            }
            p.noStroke();
            textAlign(CENTER);
            text(b.getLabel(), b.x, b.y+6);

            p.popStyle();
        };

        gui = new ProcessingGUI(this);

        gui.setLocalization("locales/en.json");

        menu = new MenuView("menu", this);
        gui.addView(menu);

        help = new HelpView("help", this);
        gui.addView(help);

        ingame = new IngameView("ingame", this);
        gui.addView(ingame);

        gui.setActiveView(menu.getLabel());
    }

    public void draw() {

    }

    public ProcessingGUI getController() {
        return gui;
    }

    public static void main(String[] args) {
        PApplet.main(Test.class.getName());
    }

}
