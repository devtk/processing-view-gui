package org.foxat.pviewgui.test;

import org.foxat.pviewgui.PGView;
import org.foxat.pviewgui.action.DrawAction;
import org.foxat.pviewgui.element.PGButton;
import org.foxat.pviewgui.interfaces.Parent;
import processing.core.PApplet;
import processing.core.PConstants;
import processing.core.PImage;

public class MenuView extends PGView {

    PGButton startBtn, helpBtn, quitBtn;
    PImage logo;

    public MenuView(String label, PApplet p) {
        super(label, p);
    }

    @Override
    public void setup() {
        Parent p = (Parent) parent;

        startBtn = new PGButton(parent.width/2f, parent.height*2/3f - 48, 200, 32, "startBtn");
        helpBtn = new PGButton(parent.width/2f, parent.height*2/3f, 200, 32, "helpBtn");
        quitBtn = new PGButton(parent.width/2f, parent.height*2/3f + 48, 200, 32, "quitBtn");

        startBtn.setLabel(p.getController().getLocaleString("startButton", "START GAME!"));
        helpBtn.setLabel(p.getController().getLocaleString("helpButton", "HOW TO PLAY"));
        quitBtn.setLabel(p.getController().getLocaleString("quitButton", "QUIT"));
        
        startBtn.setMouseReleasedAction((x, y) -> p.getController().setActiveView("ingame"));
        helpBtn.setMouseReleasedAction((x, y) -> p.getController().setActiveView("help"));
        quitBtn.setMouseReleasedAction((x, y) -> parent.exit());

        DrawAction buttonStyle = ((Test) parent).buttonStyle;

        startBtn.setStyle(buttonStyle);
        helpBtn.setStyle(buttonStyle);
        quitBtn.setStyle(buttonStyle);

        registerElement(startBtn);
        registerElement(helpBtn);
        registerElement(quitBtn);

        logo = parent.loadImage("logo.png");
    }

    @Override
    public void draw() {
        parent.background(255);
        parent.imageMode(PConstants.CENTER);
        parent.image(logo, parent.width/2f, parent.height/4f);
    }

}
