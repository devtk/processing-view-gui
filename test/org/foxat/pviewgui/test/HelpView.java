package org.foxat.pviewgui.test;

import org.foxat.pviewgui.PGView;
import org.foxat.pviewgui.element.PGButton;
import org.foxat.pviewgui.interfaces.Parent;
import processing.core.PApplet;

public class HelpView extends PGView {

    public HelpView(String label, PApplet p) {
        super(label, p);
    }

    @Override
    public void setup() {
        PGButton backBtn = new PGButton(parent.width/2f, parent.height - parent.height/4f, 120, 32, "backBtn");
        backBtn.setLabel("BACK");
        backBtn.setStyle(((Test) parent).buttonStyle);
        backBtn.setMouseReleasedAction((x, y) -> ((Parent) parent).getController().setActiveView("menu"));
        registerElement(backBtn);
    }

    @Override
    public void draw() {
        parent.background(255);
    }
}
