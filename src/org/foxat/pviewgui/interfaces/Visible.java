package org.foxat.pviewgui.interfaces;

import processing.core.PApplet;

/**
 * @author Christian Schliz
 * @since 1.0
 * */
public interface Visible {

    /**
     * InterfaceElement.draw() is called for every InterfaceObject that is visible
     *
     * @param parent the parent PApplet
     * */
    void draw(PApplet parent);

}
