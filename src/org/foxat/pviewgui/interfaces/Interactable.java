package org.foxat.pviewgui.interfaces;

import processing.core.PApplet;

/**
 * @author Christian Schliz
 * @since 1.0
 * */
public interface Interactable {
    
    /**
     * InterfaceElement.clickEvent() is called whenever a mouseClick event was triggered and the instance is visible
     *
     * @param x the mouseX coordinate
     * @param y the mouseY coordinate
     * @param parent the parent PApplet
     * */
    void clickEvent(float x, float y, PApplet parent);

    /**
     * keyDownEvent is called every draw cycle
     *
     * @param key pressed key
     * */
    void keyDownEvent(int key);

}
