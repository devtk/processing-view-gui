package org.foxat.pviewgui.interfaces;

import org.foxat.pviewgui.ProcessingGUI;

/**
 * @author Christian Schliz
 * @since 1.2
 * */
public interface Parent {

    /**
     * Enables PGViews and other classes to assume that
     * the parent object has a ProcessingGUI object.
     * */
    ProcessingGUI getController();

}
