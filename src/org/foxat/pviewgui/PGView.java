package org.foxat.pviewgui;

import org.foxat.pviewgui.action.DrawAction;
import org.foxat.pviewgui.element.PGElement;
import org.foxat.pviewgui.interfaces.Visible;
import processing.core.PApplet;

import java.util.ArrayList;

/**
 * @author Christian Schliz
 * @since 1.0
 * */
public class PGView {

    protected PApplet parent;

    ArrayList<PGElement> elements;

    @Deprecated
    DrawAction customDraw;

    final String label;

    /**
     * @param p The parent PApplet instance to interact with
     * */
    public PGView(String label, PApplet p) {
        this.parent = p;
        this.label = label;
        elements = new ArrayList<>();
        setup();
    }

    /**
     * @since 1.2
     * */
    public final String getLabel() {
        return this.label;
    }

    /**
     * This Method registers InterfaceElements for drawing and interaction
     *
     * @param x The InterfaceElement to be registered for drawing and interactions
     * */
    public void registerElement(PGElement x) {
        for(PGElement elm : elements) {
            if(elm.objectName.equalsIgnoreCase(x.objectName)) {
                new PGInterfaceException("A PGElement with the name " + x.objectName + " already exists!").printStackTrace();
                return;
            }
        }

        this.elements.add(x);
    }

    /**
     * custom pre-draw method with interfaces
     *
     * @since 1.0
     * @deprecated 1.1
     * @param x new pre() draw method
     * */
    @Deprecated
    public void registerDrawMethod(DrawAction x) {
        this.customDraw = x;
    }

    /**
     * Instead of the PApplet.draw() this or the DrawAction.draw() is used to display stuff
     *
     * @since 1.0
     * @deprecated 1.1
     * */
    @Deprecated
    public final void drawCustom() {

    }

    /**
     * Instead of the PApplet.draw() this or the DrawAction.draw() is used to display stuff
     * */
    public final void drawElements() {
        for(PGElement elm : this.elements) {
            if(elm instanceof Visible) {
                ((Visible) elm).draw(this.parent);
            }
        }
    }

    public void setup() {}

    public void draw() {}

    public void mousePressed() {}

    public void mouseReleased() {}

    public void keyPressed() {}

    public void keyReleased() {}

    public void clickEvent() {
        for(PGElement elm : this.elements) {
            assert elm != null;
            elm.clickEvent(this.parent.mouseX, this.parent.mouseY, this.parent);
        }
    }

    public void keyDownEvent() {
        for(PGElement elm : this.elements) {
            assert elm != null;
            elm.keyDownEvent(parent.keyCode);
        }
    }

}
