package org.foxat.pviewgui.action;

import org.foxat.pviewgui.element.PGButton;
import processing.core.PApplet;

/**
 * @author Christian Schliz
 * @since 1.0
 * */
public interface DrawAction {

    /**
     * This draw method is called before Interface Elements
     * are drawn, so that they are always on top.
     *
     * @since 1.0
     * @param parent parent PApplet object
     * */
    void draw(PGButton b, PApplet parent);

}
