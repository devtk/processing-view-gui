package org.foxat.pviewgui.action;

/**
 * @author Christian Schliz
 * @since 1.0
 * */
public interface MousePressedAction {

    /**
     * The event function is the same as mousePressed
     * in the PApplet object, but with arguments
     *
     * @since 1.0
     * @param x mouse x coordinate
     * @param y mouse y coordinate
     * */
    void event(float x, float y);

}
