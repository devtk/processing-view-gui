package org.foxat.pviewgui.action;

/**
 * @author Christian Schliz
 * @since 1.0
 * */
public interface KeyPressedAction {

    /**
     * The event function is the same as keyPressed
     * in the PApplet object, but with arguments
     *
     * @since 1.0
     * @param keyCode pressed key
     * */
    void event(int keyCode);

}
