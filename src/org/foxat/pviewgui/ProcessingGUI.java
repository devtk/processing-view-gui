package org.foxat.pviewgui;

import processing.core.PApplet;
import processing.data.JSONObject;
import processing.event.KeyEvent;
import processing.event.MouseEvent;

import java.util.ArrayList;

/**
 * @author Christian Schliz
 * @version 1.3
 * */
@SuppressWarnings("unused")
public class ProcessingGUI {

    PApplet parent;

    ArrayList<PGView> views;
    PGView currentView;

    JSONObject locale;

    /**
     * ProcessingGUI is the controller class
     * managing PGViews and user interaction.
     *
     * @param p In order to function properly, it needs
     *          a reference to the main PApplet object.
     * */
    public ProcessingGUI(PApplet p) {
        this.parent = p;
        views = new ArrayList<>();

        parent.registerMethod("draw", this);
        parent.registerMethod("keyEvent", this);
        parent.registerMethod("mouseEvent", this);
    }

    /**
     * Changes the active PGView by it's label
     *
     * @since         1.1
     * @param byLabel unique PGView identifier
     */
    public void setActiveView(String byLabel) {
        try {
            for(PGView view : this.views) {
                if(view.label.equals(byLabel)) {
                    this.currentView = view;
                }
            }
        } catch(Exception ex) {
            new PGInterfaceException(ex).printStackTrace();
        }
    }

    /**
     * Registers a new view
     *
     * @since          1.1
     * @param newView  the view to be added
     * */
    public void addView(PGView newView) {
        for(PGView view : this.views) {
            if(newView.label.equalsIgnoreCase(view.label) || view.equals(newView)) {
                new PGInterfaceException("Cannot register two PGViews with the same label!").printStackTrace();
                return;
            }
        }

        this.views.add(newView);
    }

    /**
     * loads a localisation file in JSON format
     *
     * @since               1.3
     * @param languageFile  File path relative to the current data folder.
     * @see                 processing.data.JSONObject
     * */
    public void setLocalization(String languageFile) {
        if(languageFile == null) {
            locale = null;
        } else {
            locale = parent.loadJSONObject(languageFile);
        }
    }

    /**
     * Retrieves a localized string
     *
     * @since               1.3
     * @param key           a key from the localisation file
     * @param defaultValue  the value being returned if either no file is loaded
     *                      or the file doesn't contain the specified key.
     * */
    public String getLocaleString(String key, String defaultValue) {
        if(locale == null) {
            return defaultValue;
        } else {
            return locale.getString(key, defaultValue);
        }
    }

    /**
     * Retrieves the whole JSONObect for no reason at all
     *
     * @since  1.3
     * @see    ProcessingGUI#getLocaleString(String, String)
     * */
    public JSONObject getLocale() {
        if(locale == null) {
            return new JSONObject();
        } else {
            return locale;
        }
    }

    @SuppressWarnings("unused")
    public void draw() {
        if(currentView != null) {
            currentView.draw();
            currentView.drawElements();
        }
    }

    @SuppressWarnings("unused")
    public void keyEvent(KeyEvent event) {
        if(currentView != null) {
            if(event.getAction() == KeyEvent.PRESS) {
                currentView.keyPressed();
                currentView.keyDownEvent();
            } else if(event.getAction() == KeyEvent.RELEASE) {
                currentView.keyReleased();
            }
        }
    }

    @SuppressWarnings("unused")
    public void mouseEvent(MouseEvent event) {
        if(currentView != null) {
            if(event.getAction() == MouseEvent.PRESS) {
                currentView.mousePressed();
            } else if(event.getAction() == MouseEvent.RELEASE) {
                currentView.mouseReleased();
                currentView.clickEvent();
            }
        }
    }
}
