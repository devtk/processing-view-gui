package org.foxat.pviewgui.element;

import org.foxat.pviewgui.action.DrawAction;
import org.foxat.pviewgui.interfaces.Visible;
import processing.core.PApplet;
import processing.core.PConstants;

/**
 * @author Christian Schliz
 * @since 1.0
 * */
@SuppressWarnings("unused")
public class PGButton extends PGElement implements Visible {

    /**
     * text inside the button
     * */
    protected String label = "";

    /**
     * custom draw action that overrides the PGButton.draw()
     * */
    private DrawAction customDraw;

    /**
     * @param x          x coordinate
     * @param y          y coordinate
     * @param w          width
     * @param h          height
     * @param objectName object identifier
     */
    public PGButton(float x, float y, float w, float h, String objectName) {
        super(x, y, w, h, objectName);
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    /**
     * Sets a custom DrawAction to replace the PGButton.draw()
     * */
    public void setStyle(DrawAction x) {
        this.customDraw = x;
    }

    /**
     * InterfaceElement.draw() is called for every InterfaceObject that is visible
     *
     * @param parent the parent PApplet
     */
    @Override
    public void draw(PApplet parent) {
        if(this.customDraw != null) {
            customDraw.draw(this, parent);
        } else {
            parent.pushStyle();
            parent.rectMode(PConstants.CENTER);

            if (parent.mousePressed && this.touchUpInside(parent.mouseX, parent.mouseY)) {
                parent.fill(64);
            } else {
                parent.fill(220);
            }

            parent.strokeWeight(2);
            parent.stroke(64);
            parent.rect(this.x, this.y, this.w, this.h, PApplet.min(w, h) / 4f);

            parent.textAlign(3, 3);
            parent.textSize(16);

            if (parent.mousePressed && this.touchUpInside(parent.mouseX, parent.mouseY)) {
                parent.fill(255);
            } else {
                parent.fill(0);
            }

            parent.noStroke();
            parent.text(this.label, this.x + 2, this.y + 2);
            parent.popStyle();
        }
    }

}
