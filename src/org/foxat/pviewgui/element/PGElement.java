package org.foxat.pviewgui.element;

import org.foxat.pviewgui.ProcessingGUI;
import org.foxat.pviewgui.action.KeyPressedAction;
import org.foxat.pviewgui.action.MousePressedAction;
import org.foxat.pviewgui.action.MouseReleasedAction;
import org.foxat.pviewgui.interfaces.Interactable;
import processing.core.PApplet;

/**
 * @author Christian Schliz
 * @since 1.0
 * */
@SuppressWarnings("unused")
public abstract class PGElement implements Interactable {

    /**
     * dimensions of the element
     * */
    public float x, y, w, h;

    /**
     * name of the object
     * */
    public final String objectName;

    /**
     * The visibility setting determines, whether the object should be drawn and/or interacted with.
     * */
    boolean visible;

    /**
     * The z-level sets this objects rank in the drawing order, making objects with higher z-level appear on top.
     * */
    int zLevel;

    private MouseReleasedAction releasedActionM;
    private KeyPressedAction pressedActionK;

    /**
     * @param x x coordinate
     * @param y y coordinate
     * @param w width
     * @param h height
     * */
    public PGElement(float x, float y, float w, float h, String objectName) {
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
        this.objectName = objectName;

        this.visible = true;
        this.zLevel = 0;
    }

    /**
     * Sets the pressedActionM variable as a callable interface
     * @param x called interface
     * */
    public void setMousePressedAction(MousePressedAction x) {
        // TODO: Implement mousePressed for PGElements
    }

    /**
     * Sets the releasedActionM variable as a callable interface
     * @param x called interface
     * */
    public void setMouseReleasedAction(MouseReleasedAction x) {
        this.releasedActionM = x;
    }

    /**
     * Sets the releasedActionM variable as a callable interface
     * @param x called interface
     * */
    public void setKeyPressedAction(KeyPressedAction x) {
        this.pressedActionK = x;
    }


    /**
     * Changes the position of the element
     *
     * @param x new x position
     * @param y new y position
     * */
    public void transform(float x, float y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Changes the position and size of the element
     *
     * @param x x position
     * @param y y position
     * @param w width
     * @param h height
     * */
    public void transform(float x, float y, float w, float h) {
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
    }

    /**
     * InterfaceElement.clickEvent() is called whenever a mouseClick event was triggered and the instance is visible
     *
     * @param x the mouseX coordinate
     * @param y the mouseY coordinate
     * @param parent the parent PApplet
     */
    @Override
    public void clickEvent(float x, float y, PApplet parent) {
        if(this.touchUpInside(x, y)) {
            if(this.releasedActionM == null) {
                System.err.println("Element with name \"" + this.objectName + "\" is using the default action method!");
            } else {
                this.releasedActionM.event(x, y);
            }
        }
    }

    /**
     * keyDownEvent is called every draw cycle
     *
     * @param key pressed key
     */
    @Override
    public void keyDownEvent(int key) {
        if(this.pressedActionK == null) {
            // System.err.println("Element with name \"" + this.objectName + "\" is using the default action method!");
        } else {
            this.pressedActionK.event(key);
        }
    }

    public boolean touchUpInside(float x, float y) {
        return x >= this.x - this.w/2 && x <= this.x + this.w/2 && y > this.y - this.h/2 && y < this.y + this.h/2;
    }

    /**
     * Setter for the visibility setting
     * @param visible new value
     * */
    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    /**
     * Setter for the z-level setting
     * @param zLevel new value
     * */
    public void setZLevel(int zLevel) {
        this.zLevel = zLevel;
    }

}
