# Processing View-Based GUI Library

This processing library enables the use of view based interface structures within processing,
while retaining its function to draw to the canvas directly.

## Installation

This library is provided as a jar file.
To include it in the Processing IDE, please do one of the following:

- Import the jar file. (Sketch > Add File...)
- Create a folder named `code` inside the project directory, that contains the jar file.

## Usage

Please see the builtin Javadoc comments for more information.

The `ProcessingGUI` object handles all views and interfaces.
It contains a list of `PGView`s.
To create a view, simply create a subclass of `PGView` and implement
methods like `draw()`, `keyPressed()` or `mousePressed()`.

A small example could look like this;

```Java
import org.foxat.pviewgui.*;import org.foxat.pviewgui.action.MouseReleasedAction;

ProcessingGUI gui;
DefaultView main;

void setup() {
  size(400, 400);
  gui = new ProcessingGUI(this);
  main = new DefaultView("main", this);
  gui.addView(main);
  gui.setActiveView(main.label);
}

void draw() {
  // this can be left empty, the library utlizes PApplet.registerMethod()
}

// This is where you want to put your actual interface building
class DefaultView extends PGView {
  
  protected String label;
  
  public DefaultView(String label, PApplet p) {
    super(label, p);
    this.label = label;
    this.setup();
  }
  
  void setup() {
    PGButton btn = new PGButton(200, 200, 150, 30, "button1"); // new Button object
    btn.setLabel("Click Me!"); // label inside the button
    btn.setMouseReleasedAction(new MouseReleasedAction() { // same as mouseReleased()
      @Override
      void action(float x, float y) {
        PApplet.println("Mouse clicked at X:" + x + " Y:" + y);
      }
    });
    registerElement(btn); // tells the view do display the button
  }
  
  void draw() {
    background(51);
  }
  
}
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

```
Copyright © 2020 by Christian Schliz. All rights reserved.
```